import { createRouter, createWebHistory } from "vue-router"

import Details from "../pages/Details.vue"
import Home from "../pages/Home.vue"

const routes = [
    {
        path: "/",
        component: Home
    },

    {
        path: "/details/:id",
        component: Details
    }
]

const router = createRouter({
    history: createWebHistory("/my-blog"),
    routes
})

export default router;