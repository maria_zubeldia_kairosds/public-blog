import axios from "axios";

export class PostsProxyService {

    BASE_URL = `https://local-apps.kairosds.com`

    POSTS_URL = `${BASE_URL}/api/posts`
    COMMENT_URL = `${BASE_URL}/api/posts`
    
    CONFIG = {
        headers: {
          "Content-Type": "application/json",
        },
      };

    async getPosts() {

        const response = await axios.post(`${POSTS_URL}`, CONFIG)
        console.log("proxy",response.data);
        return response.data;
    }


}
