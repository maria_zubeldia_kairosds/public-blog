FROM node:16-alpine3.14 as builder
RUN mkdir /my-blog
WORKDIR /my-blog
COPY . .
RUN npm ci
RUN npm run build
RUN npm prune --production

FROM nginx:1.23.1-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /my-blog/dist/ /usr/share/nginx/html/my-blog
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
